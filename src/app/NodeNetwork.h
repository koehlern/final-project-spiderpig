#pragma once
#include "utils/mathUtils.h"
#include <set>

class Shader;
class Ray;


// different edit modes that are needed in the editor of the node network
enum class editMode { disable, addNodes, connectNodes };

// a node primarily gets used when constructing a shortest path
struct node {
  int id;
  P3D location;
  int best_predecessor = -1;
  double current_cost = std::numeric_limits<double>::max();
  double heuristic_cost = std::numeric_limits<double>::max();

  node(int id_, P3D location_) : id(id_), location(location_) {}

  // compare for the sorting in the std::set
  // if both nodes have the same ID, they are considered the same nodes
  // otherwise we sort by total (expected) cost
  bool operator<(const node& nd) const {
    // if we are the same node, we should compare the same!
    if (id == nd.id) {
      return false;
    }

    else if (current_cost + heuristic_cost == nd.current_cost + heuristic_cost)
      return id < nd.id;

    return (current_cost + heuristic_cost < nd.current_cost + nd.heuristic_cost);
  }
};

// the NodeNetwork manages all things related to the navigation graph
class NodeNetwork
{
public:
  NodeNetwork(std::string file_path = CRL_DATA_FOLDER"/network/example.txt") : filename(file_path) {
    load();
  }

  // load and save with the text file
  void load();
  void save();

  // ui functions
  void hover(Ray& mouseRay);
  void click();
  void draw(const Shader& rbShader);

  // edit network functions
  void add(P3D& location);
  void remove(int node);
  void connect(int node_a, int node_b, double cost = 0);

  // returns a shortest path on the network between the two points
  std::vector<int>& getShortestPath(const P3D& start, const P3D& end);

  // helper functions for A*
  node updateNode(node& parent, int id_);
  void explore_node(node& n);
  
  double getCosts(int node_a, int node_b);

  editMode mode = editMode::disable;
  std::vector<node> nodes;

  float node_draw_radius = 0.1;

private:

  Eigen::MatrixXd costs;

  P3D goal;
  P3D network_goal;
  P3D mouseLocation;
  int hovered_Node = -1;
  int selected_Node = -1;
  const std::string filename;

  // stuff for A*
  std::set<node> known_nodes;
  std::vector<bool> unknown_nodes;
  std::vector<bool> finished_nodes;
  std::vector<int> path;
  std::vector<bool> in_path;
};

