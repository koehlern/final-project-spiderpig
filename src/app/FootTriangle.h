#ifndef FOOTTRIANGLE_H
#define FOOTTRIANGLE_H

#include "utils/mathUtils.h"
#include "SpiderPig.h"

template <typename T> int sgn(T val) {
  return (T(0) < val) - (val < T(0));
}

class FootTriangle {
  
public:

  double& max_step_length;
  double current_time = 0;
  double& step_duration;
  double& feet_radius;

  double angle_step_size = 0.2;
  const double foot_high = -0.03;
  const double foot_low = -0.05;
  double foot_lifted_y = -0.1;
  double foot_ground_y = -0.1;

  std::vector<V3D> A_base;
  std::vector<V3D> B_base;

  // state of our triangle
  // max velocity is 1/step_duration, because if we have max velocity for a time
  // of step_duration, then we moved 1 (which in turn is what we want).
  // [0,2]
  V3D rel_velocity = V3D(0, 0, 0);
  // we flip position when changing stance
  // [0,1]
  V3D rel_position = V3D(0,0,0);
  // V3D rel_offset = V3D(0, 0, 0);
  V3D glob_offset = V3D(0, 0, 0);

  double front_rotation = 0;
  double x_scale = 1;
  double z_scale = 1;

  double rel_angle = 0;
  double rot_velocity = 0;
  bool in_step_A = true;

  V3D abs_position() const {
    return rel_position / feet_radius;
  }
    
  FootTriangle(SpiderPig& pig) : max_step_length(pig.step_size), step_duration(pig.step_duration), feet_radius(pig.radius) {
    V3D radius = V3D(-1, 0, 0 );

    A_base.push_back(rotateVec(radius, pig.angles_A[0], pig.robot.up()));
    A_base.push_back(rotateVec(radius, pig.angles_A[1], pig.robot.up()));
    A_base.push_back(rotateVec(radius, pig.angles_A[2], pig.robot.up()));
      
    B_base.push_back(rotateVec(radius, pig.angles_B[0],pig.robot.up()));
    B_base.push_back(rotateVec(radius, pig.angles_B[1], pig.robot.up()));
    B_base.push_back(rotateVec(radius, pig.angles_B[2], pig.robot.up()));
  }

  std::vector<V3D> getAPositions(bool lifted = true) const {
    std::vector<V3D> ret;
    if (lifted)
      lifted = in_step_A;

    for (const V3D& location : A_base) {
      V3D vect;
      if (in_step_A)
        vect = feet_radius * rotateVec(location, rel_angle * angle_step_size, V3D(0, -1, 0)) + rel_position * max_step_length;
      else
        vect = feet_radius * rotateVec(location, -rel_angle * angle_step_size, V3D(0, -1, 0)) - rel_position * max_step_length;

      
      vect[1] = lifted ? foot_lifted_y : foot_ground_y;
      vect = rotateVec(vect, front_rotation, V3D(1, 0, 0));
      vect += glob_offset;
      vect =vect.cwiseProduct(V3D(x_scale, 1, z_scale)) ;
      ret.push_back(vect);
    }
    return ret;
  }

  std::vector<V3D> getBPositions(bool lifted = true) const {
    std::vector<V3D> ret;
    if (lifted)
      lifted = !in_step_A;
    for (const V3D& location : B_base) {
      V3D vect;
      if (in_step_A)
       vect = feet_radius * rotateVec(location, -rel_angle * angle_step_size, V3D(0, -1, 0)) - rel_position * max_step_length;
      else  
        vect = feet_radius * rotateVec(location, +rel_angle * angle_step_size, V3D(0, -1, 0)) + rel_position * max_step_length;
      vect[1] = lifted ? foot_lifted_y : foot_ground_y;
      vect = rotateVec(vect, front_rotation, V3D(1, 0, 0));
      vect += glob_offset;
      vect = vect.cwiseProduct(V3D(x_scale, 1, z_scale));
      ret.push_back(vect);
    }
    return ret;
  }

  bool is_resting() {
    return (rel_position.norm() < 0.3 && abs(rel_angle) < 0.3
      && rel_velocity.norm() < 0.3 && abs(rot_velocity) < 0.3);
  }

  void fade_velocity(double velocity, double dt) {
    
  }

  void fade_rel_angle(double velocity, double dt) {
    if (abs(rel_angle) < velocity * dt)
      rel_angle = 0;
    else
      rel_angle = rel_angle - sgn(rel_angle) * velocity * dt;
  }

  void fade_scale(double velocity, double dt) {
    double diff = 1 - x_scale;
    if (abs(diff) < velocity * dt)
      diff = 0;
    else
      diff = diff - sgn(diff) * velocity * dt;
    x_scale = 1 - diff;

    diff = 1 - z_scale;
    if (abs(diff) < velocity * dt)
      diff = 0;
    else
      diff = diff - sgn(diff) * velocity * dt;
    z_scale = 1 - diff;
  }

  void fade_global_offset(double velocity, double dt) {
    if (glob_offset.norm() < velocity * dt)
      glob_offset = Vector3d::Zero();
    else
      glob_offset = glob_offset - (glob_offset.normalized() * velocity * dt);

  }

  void fade_relative_position(double velocity, double dt) {
    if (rel_position.norm() < velocity * dt)
      rel_position = Vector3d::Zero();
    else
      rel_position = rel_position - (rel_position.normalized() * velocity * dt);

  }

  void fade_feet_difference(double velocity, double dt) {
    double req_feet_y_velocity = velocity * 0.1;
    double lifted_feet_diff = foot_lifted_y - foot_low;
    double lower_feet_diff = foot_ground_y - foot_low;

    if (abs(lifted_feet_diff) < req_feet_y_velocity * dt)
      foot_lifted_y = foot_low;
    else
      foot_lifted_y = foot_lifted_y - sgn(lifted_feet_diff) * req_feet_y_velocity * dt;

    if (abs(lower_feet_diff) < req_feet_y_velocity * dt)
      foot_ground_y = foot_low;
    else
      foot_ground_y = foot_ground_y - sgn(lower_feet_diff) * req_feet_y_velocity * dt;
  }

 

  void time_step_idle(double dt) {
    // if we receive no input, we should lower the legs and center the body
    // in the step, we are at time 0
    current_time = 0;
    rel_velocity = 0;
    rot_velocity = 0;

    // we based the centering on the timing of the steps
    double center_in = 3 * step_duration;
    
    double velocity = 1.0 / center_in;

    

    // go towards 0
    fade_relative_position(velocity, dt);
    fade_global_offset(velocity, dt);
    fade_rel_angle(velocity, dt);
    fade_feet_difference(velocity, dt);
  }

  void time_step_auto (const V3D& input_direction, double input_angle, double dt, double input_threshold) {
    if (input_direction.norm() > input_threshold || abs(input_angle) > input_threshold || !is_resting()) {
      time_step_walk(input_direction, input_angle, dt);
    }
    else {
      time_step_idle(dt);
    }
  }

  void time_step_auto(double dt) {
    time_step_auto(Vector3d{ 0,0,0 }, 0, dt, 0.2);
  }

  // update goal, interpolate from current position to the new goal
  // based on elapsed time and step size. Save this as current position. for smooth control we also need dx and dr, they tell
  void time_step_walk(const V3D& input_direction, double input_angle, double dt)
  {    
    // where are we concerning our step timing?
    current_time += dt;

    if (current_time >= step_duration)
      next_step();

    double k = current_time / step_duration;

    // back-polate where we would have started the step if we had the requested velocity for the whole step (2.0 is just a bonus factor)
    V3D step_start = rel_position - (input_direction * k * 2);

    // we want to adjust velocity based on this:
    auto goal_rel_velocity = (input_direction - step_start);

    if (goal_rel_velocity.norm() > 2)
      goal_rel_velocity = goal_rel_velocity.normalized() * 2;

    // linear velocity ramp up
    // with maximum input, we want to need more than one step duration to get to full velocity
    // full velocity is 2/step_duration * step_duration = 2 
    // test ramp up:
    auto dv_max = 2*dt/step_duration;
    auto rel_velocity_difference = goal_rel_velocity - rel_velocity;
    if (rel_velocity_difference.norm() < dv_max)
      rel_velocity = goal_rel_velocity;
    else
      rel_velocity += rel_velocity_difference.normalized() * dv_max;

    rel_position += (rel_velocity / step_duration * dt);


    // rotation is pretty similar to translation
    // where would we have started with the rotation if we had the requested turn rate for the whole step?
    double rot_start = rel_angle - (input_angle * k * 2);
    double goal_rot_velocity = input_angle - rot_start;

    if (abs(goal_rot_velocity) > 2) {
      goal_rot_velocity = sgn(goal_rot_velocity) * 2;
    }

    auto dr_max = 2 * dt / step_duration;
    auto rel_rot_velocity_difference = goal_rot_velocity - rot_velocity;

    if (abs(rel_rot_velocity_difference) < dr_max)
      rot_velocity = goal_rot_velocity;
    else
      rot_velocity += sgn(rel_rot_velocity_difference) * dr_max;

    rel_angle += (rot_velocity / step_duration * dt);

    // now, let us take care of y displacement of the foots
    double fac = (k * 2) - 1;
    foot_lifted_y = foot_high + (fac * fac) * (foot_low - foot_high);
    foot_ground_y = foot_low;

    fade_global_offset(1.0 / (3.0 * step_duration), dt);
  }

  
  void time_step_jump(double velocity, double dt) {
    V3D vel = { 0, -velocity,0 };
    glob_offset += vel * dt;
  }
 
  void time_step_precise_jump_extending(double flip_angle, const V3D& requested_velocity, double dt) {
    glob_offset -= requested_velocity * dt;
    front_rotation = flip_angle;
  }

  void time_step_move_cg(V3D goal, double dt) {
    
    goal.y() = 0;
    glob_offset -= goal*dt*10;
  }

  void reset() {
    rel_angle = 0;
    rel_position = Vector3d::Zero();
    rel_velocity = Vector3d::Zero();
    glob_offset = Vector3d::Zero();

    foot_ground_y = foot_low;
    foot_lifted_y = foot_high;
  }

  void next_step() {
    in_step_A = !in_step_A;
    current_time -= step_duration;
    rel_position = -rel_position;
    rel_angle = -rel_angle;
  }
};


#endif // FOOTTRIANGLE_H
