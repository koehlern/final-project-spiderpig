#ifndef SPIDERPIG_H
#define SPIDERPIG_H

#include "robot/Robot.h"
#include "../../build/src/app/timing.h"


class NodeNetwork;
class SpiderState;
class FootTriangle;

class SpiderPig {

public:
  SpiderPig(RBEngine* rbEngine, Vector3d& input, double& angle_control_input, double& _step_size, double& _step_time, double& _radius, timing& timer_, bool& autopause);

  // called every time step, actual processing handled in state
  void process();

  // helper functions
  void get_current_angles(Eigen::VectorXd& angles);
  void get_default_angles(Eigen::VectorXd& angles);
  void set_current_angles(Eigen::VectorXd& angles);

  void set_specific_angle(size_t index, double angle);
  void set_specific_angles(size_t count, size_t* index, double* angle);

  Vector3d to_local(const Vector3d& global_coord);
  Vector3d to_global(const Vector3d& local_coord);

  // IK functions
  Eigen::Vector3d IK_angles_rel(size_t leg, P3D goal);
  Eigen::Vector3d IK_angles_abs(size_t leg, P3D goal);
  P3D getWorldCoordinates(const P3D& pLocal) const { return robot.root->state.getWorldCoordinates(pLocal); }
  void IK_set_foot_lcl(size_t leg, V3D goal);

  // force transitions (delete the old chain)
  void to_idle();
  void to_pause();
  void to_default(double trans_time);
  void to_oneLegIK(Vector3d& goal, size_t leg);
  
  void to_directControl();
  void to_jump(double velocity);
  void to_precise_jump(Vector3d& goal);
  void to_move_towards(Vector3d& goal);
  void to_walk_to_target(P3D goal, NodeNetwork& network_);
  void to_rotate_towards(Vector3d& goal);
  void to_flip(double flip_velocity);


  // clean-up next states
  void purge_states();

  // default transition
  void to_next();

  // handy joint indices
  // ordered by frontR, then frontL and so on
  const size_t i_legs[6] = { 0,1,3,2,4,5 };
  const size_t i_A_legs[3] = { 0,3,4 };
  const size_t i_B_legs[3] = { 1,2,5 };

  // offsets from base joint
  const size_t i_joints[3] = { 0,7,13 };
  const size_t i_head = 6;

  // handy rigid body lengths
  const double cen_l = 0.055;
  const double top_l = 0.05;
  const double mid_l = 0.0885;
  const double end_l = 0.155;

  // further configuration info
  const double angles_A[3] = { .25 * PI, PI , -.25*PI };//{ 0.25 * PI, -0.5 * PI, 0.75 * PI };
  const double angles_B[3] = { .75* PI, 0, -0.75 * PI };

  // current foot placement triangle
  FootTriangle* triangle;

  // current input
  Vector3d& control_input;
  double& angle_input;
  double& step_size;
  double& step_duration;
  double& radius;

  const Vector3d zero_input = Vector3d::Zero();
  const double zero = 0;

  timing& timer;
  
  double universal_time = 0;

  Robot robot;
  RBState& rb_state;
  bool schedule_pause = false;
  bool& enable_autopause;
  RBEngine* rbEngine;

  // state design:
  // state is always the current state
  // idle state is the unique state where the robot idles, it should never be deleted, it is also the default next_state of every new state
  // top_state is the root of the state tree, if we purge the states we start there
  // in the states there are next_states, parent_states and child_states
  // it is illegal to have both a parent_state and a next_state
  // we delete trough next_states, but not to parent_states
  // child states get deleted of course!
  SpiderState* state;
  SpiderState* top_state = nullptr;
  SpiderState* idle_state;
  size_t n_states = 0;

  const size_t angle_length;


private:
  bool advance_state;
};
#endif // SPIDERPIG_H
