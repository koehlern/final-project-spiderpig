#ifndef SPIDERSTATE_H
#define SPIDERSTATE_H

#include "SpiderPig.h"
#include "FootTriangle.h"
#include "NodeNetwork.h"

// returns a 2d vector by the rotation to a 0 z component
inline Vector2d to_2D_problem (const Vector3d& vector) {
  return Vector2d(sqrt(vector.x() * vector.x() + vector.z() * vector.z()), vector.y());
}

// A spider state implements higher level tasks that a spider can do,
// they get chained in a list. Each state knows the next state, so we can 
// easily add links in the chain.
class SpiderState {
public:

  SpiderState(SpiderPig& _pig,
    double end_time = std::numeric_limits<double>::max()) : pig(_pig), elapsed_t(0), end_t(end_time), triangle(*_pig.triangle), timer(_pig.timer) {
    pig.n_states += 1;
  }

  virtual ~SpiderState() {
    assert(running == false);

    // we always have a next_state, so this is safe    
    delete next_state;

    // we are responsible to delete our children
    delete child_state;

    //Logger::consolePrint("%d: Deleted state:  %s ", pig.n_states, this->name.c_str());
    pig.n_states -= 1;
  }

  // gets called when we activate a state. first of the base class, and then the derived class might
  // implement the local version
  void enter() {
    assert(running == false);
    running = true;
    Logger::consolePrint("%d: Entering state:  %s ", pig.n_states, this->name.c_str());
    enter_local();
  }

  // gets called every simulation time step. first of the base class, and then the derived class might
  // implement the local version
  void process() {
    assert(running == true);

    pig.set_specific_angle(pig.i_head, sin(pig.universal_time));
    if (elapsed_t > end_t) {
      pig.to_next();
      pig.process();
      return;
    }

    process_local();

    // set motor angles
    for (int i = 0; i < pos_A.size(); i++) {
      pig.IK_set_foot_lcl(pig.i_A_legs[i], pos_A[i]);
      pig.IK_set_foot_lcl(pig.i_B_legs[i], pos_B[i]);
    }
    elapsed_t += timer.get_dt();
  }

  // gets called when we leave the state. first of the base class, and then the derived class might
  // implement the local version
  void exit() {
    assert(running == true);
    running = false;
    Logger::consolePrint("%d: Exiting state:  %s ", pig.n_states, this->name.c_str());
    exit_local();
  }

  SpiderPig& pig;
  // how much time has elapsed, and how long should we stay in this state in total?
  double elapsed_t;
  const double end_t;

  std::string name = "Base State: Do not use!";

  // pointers for the state machine
  SpiderState* next_state = nullptr;
  SpiderState* child_state = nullptr;
  SpiderState* parent_state = nullptr;

  // will provide us with positions for our feet
  FootTriangle& triangle;

  // some state have to change the timing (jump states)
  timing& timer;

  // positions for A and B feet
  std::vector<V3D> pos_A;
  std::vector<V3D> pos_B;

private:
  // helps with debuggin scheduling issues
  bool running = false;

  virtual void enter_local() {
    // do nothing else
  }

  virtual void process_local() {
    std::cout << "Invalid: process called on base-state!\n";
    assert(false && "Invalid: process called on base-state!");
  };

  virtual void exit_local() {
    // do nothing else
  }
};

// Clear namespace for nice and unique names
namespace SpiderPigState
{
  // unique idle state, will be always ready from the spider pig
  class Idle : public SpiderState {
  public:

    Idle(SpiderPig& pig) : SpiderState(pig) {
      name = "Idle";
    }

    ~Idle() {
      assert(false && "ERROR: Deleting unique Idle state is not allowed");
    }

  private:

     void process_local() override {

      triangle.time_step_auto(Vector3d{ 0,0,0 }, 0, timer.get_dt(), 0.2);

      pos_A = triangle.getAPositions();
      pos_B = triangle.getBPositions();
    }
  };

  class Rest : public SpiderState {
  public:

    Rest(SpiderPig& pig, double duration = std::numeric_limits<double>::max()) : SpiderState(pig, duration) {
      name = "Resting";
    }

  private:
    void process_local() override {
      triangle.time_step_idle(timer.get_dt());
      
      pos_A = triangle.getAPositions();
      pos_B = triangle.getBPositions();
    }
  };

  class Pause : public SpiderState {
  public:

    Pause(SpiderPig& pig, double duration = std::numeric_limits<double>::max()) : SpiderState(pig, duration) {
      name = "Pausing";
    }

  private:
    void process_local() override {
      pos_A = triangle.getAPositions();
      pos_B = triangle.getBPositions();
    }
  };

  // Activate IK on one leg
  class OneLegIK : public SpiderState {
  public:

    OneLegIK(SpiderPig& pig, Vector3d& goal, size_t leg) : SpiderState(pig), goal_(goal), leg_(leg) {
      name = "One Leg IK";
    }

    void process_local() override {

      // get angles
      auto angles = pig.IK_angles_abs(leg_, getP3D(goal_));

      // set angles
      if (leg_ < 3) {
        pos_A[leg_] = angles;
      }
      else
        pos_B[leg_ - 3] = angles;
    }

  private:
    const Vector3d& goal_;
    const size_t leg_;
  };

  // move pig based on direct control input, this is a super state
  class directControl : public SpiderState {
  public:
    directControl(SpiderPig& pig, Vector3d& control_input_, double& angle_input_, double end_time = std::numeric_limits<double>::max()) : SpiderState(pig, end_time), control_input(control_input_), angle_input(angle_input_) {
      name = "Direct Control Master State";
    }

    directControl(SpiderPig& pig, double end_time = std::numeric_limits<double>::max()) : SpiderState(pig, end_time), control_input(pig.zero_input), angle_input(pig.zero) {
      name = "Direct Control Master State with 0 input (defaulting)";
    }

    void process_local() override {
      triangle.time_step_auto(control_input, angle_input, timer.get_dt(), 0.2);
      
      // lerp triangles and get positions
      pos_A = triangle.getAPositions();
      pos_B = triangle.getBPositions();      
    }

  private:
    const Vector3d& control_input;
    const double& angle_input;
  };


  class rotateTowards :public SpiderState {
  public:
    rotateTowards(SpiderPig& pig, const Vector3d& _target, bool quit_when_close_ = false) : SpiderState(pig), target(_target), quit_when_close(quit_when_close_) {
      name = "Rotating towards Target";
    }

    void process_local() override {
      // first rotate target to align reference frame
      double pig_rotation = pig.robot.getHeadingAngle();
      Vector3d diff = target - V3D(pig.robot.computeCOM());
      Vector3d rel_target = rotateVec(diff, pig_rotation, { 0,-1,0 });

      // simulate input
      double input_angle = planeAngleBetween({ 0,0,1 }, rel_target, { 0,1,0 });

      if (input_angle > PI)
        input_angle -= 2 * PI;

      input_angle *= 5;

      input_angle = sgn(input_angle) * input_angle * input_angle;
      if (abs(input_angle) > 1)
        input_angle = sgn(input_angle);

      triangle.time_step_auto({ 0, 0, 0 }, -input_angle, timer.get_dt(), 0.001);

      // lerp triangles and get positions
      pos_A = pig.triangle->getAPositions();
      pos_B = pig.triangle->getBPositions();      

      if (quit_when_close && abs(input_angle) < 0.001)
        pig.to_next();

    }

    const Vector3d& target;
    const bool quit_when_close;
  };

  class moveTowards :public SpiderState {
  public:
    moveTowards(SpiderPig& pig, const Vector3d& _target, bool quit_when_close_ = false, double tolerance_ = 0.1) : SpiderState(pig), target(_target), quit_when_close(quit_when_close_), tolerance(tolerance_) {
      name = "Moving towards Target";
    }

    void process_local() override {

      double pig_rotation = pig.robot.getHeadingAngle();
      Vector3d diff = target - V3D(pig.robot.computeCOM());
      Vector3d rel_target = rotateVec(diff, pig_rotation, { 0,-1,0 });

      // simulate input
      double input_angle = planeAngleBetween({ 0,0,1 }, rel_target, { 0,1,0 });

      if (input_angle > PI)
        input_angle -= 2 * PI;

      input_angle *= 5;

      input_angle = sgn(input_angle) * input_angle * input_angle;
      if (abs(input_angle) > 1)
        input_angle = sgn(input_angle);

      // input is a vector in the direction of the target
      Vector3d input_direction = { rel_target.x() * 5, 0 , rel_target.z() * 5 };
      if (input_direction.norm() > 1)
        input_direction.normalize();

      if (input_direction.norm() < 0.1)
        input_angle = 0;

      triangle.time_step_auto(input_direction, -input_angle, timer.get_dt(), 0.1);

      // lerp triangles and get positions
      pos_A = pig.triangle->getAPositions();
      pos_B = pig.triangle->getBPositions();
            
      if (quit_when_close && diff.norm() < tolerance)
        pig.to_next();
    }

  private:
    const Vector3d& target;
    const bool quit_when_close;
    const double tolerance;
  };

  class adjustCG :public SpiderState {
  public:
    adjustCG(SpiderPig& pig, Vector3d global_goal) : SpiderState(pig), goal_cg(global_goal) {
      name = "Adjust CG";
    }
    
    void process_local() override {
      P3D adjusted_goal = goal_cg + pig.rb_state.getWorldCoordinates(V3D(0, 0, 0.00));
      V3D local_goal = V3D(pig.rb_state.getLocalCoordinates(adjusted_goal));
      
      // move com there by just doing the relative opposite motion with all the feet
      local_goal.y() = 0;
      triangle.time_step_move_cg(local_goal, timer.get_dt());

      // lerp triangles and get positions
      pos_A = pig.triangle->getAPositions();
      pos_B = pig.triangle->getBPositions();
            
      // exit if we reached our goal
      if (V3D(local_goal).norm() < 0.00001)
        pig.to_next();
    }

    const P3D goal_cg;
  };

  class jump :public SpiderState {
  public:
    jump(SpiderPig& pig, double goal_velocity_ = 10) : SpiderState(pig, 0.2/goal_velocity_), goal_velocity(goal_velocity_), goal_acceleration(goal_velocity*goal_velocity_/0.05) {
      name = "Jump";
    }

    void enter_local() override {
      timer.adjust(goal_velocity, 30);

      RobotRB* ground = pig.rbEngine->getRBByName("ground");
      ground->rbProps.frictionCoeff = 1000;
      
    }

    void process_local() override {
      auto actual_velocity = pig.robot.computeCOMVelocity();
      if (actual_velocity.y() < goal_velocity) {
        double velocity = goal_acceleration * elapsed_t;

        triangle.time_step_jump(velocity, timer.get_dt());

        // lerp triangles and get positions
        pos_A = pig.triangle->getAPositions();
        pos_B = pig.triangle->getBPositions();                
      }
      else {
        pig.to_next();
      }
    }

    void exit_local() override {
      auto v = pig.robot.computeCOMVelocity();
      Logger::consolePrint("%d: Jump completed with velocity %f of %f requested.", pig.n_states, v.y(), goal_velocity);

      RobotRB* ground = pig.rbEngine->getRBByName("ground");
      ground->rbProps.frictionCoeff = 0.8;
      timer.reset();
    }

  private:
    const double goal_velocity;
    const double goal_acceleration;
  };

  class Extend : public SpiderState {
  public:

    Extend(SpiderPig& pig, double req_velocity) : SpiderState(pig), requested_magnitude(req_velocity) {
      name = "Extending for precise jump";
    }

  private:
    void enter_local() override {
      ramp_time = timer.adjust(max_velocity, 200);
      RobotRB* ground = pig.rbEngine->getRBByName("ground");
      ground->rbProps.frictionCoeff = 10000;
    }

    void process_local() override {
      // we have a defined number of timesteps where we ramp up the velocity
      // based on this, we project what velocity we would have at the end
      // we then adjust our goal in the opposite direction, and ramp up again one step
      double k = elapsed_t / ramp_time;
      double kp1 = (elapsed_t + timer.get_dt()) / ramp_time;
      

      V3D v = pig.robot.computeCOMVelocity();
      if (v.norm() > requested_magnitude) {
        pig.to_next();
        return;
      }

      if (k > 1) {
        pig.to_next();
        return;
      }

      double current_req_magnitude = kp1 * max_velocity;
      double current_flip_angle = kp1 * -0.07;
      V3D current_req_velocity = V3D(ideal_push_direction.normalized() * current_req_magnitude);

      triangle.time_step_precise_jump_extending(0, current_req_velocity, timer.get_dt());

      // lerp triangles and get positions
      pos_A = triangle.getAPositions();
      pos_B = triangle.getBPositions();

      elapsed_timesteps++;      
    }

    void exit_local() override {
      V3D v = pig.robot.computeCOMVelocity();
      Logger::consolePrint("Extension completed after %d timesteps with a velocity of %f of  %f.", elapsed_timesteps, v.norm(), requested_magnitude);
      timer.reset();
      RobotRB* ground = pig.rbEngine->getRBByName("ground");
      ground->rbProps.frictionCoeff = 0.8;
    }

    const double max_velocity = 25;
    const double requested_magnitude;
    double ramp_time;
    size_t elapsed_timesteps = 0;
    const V3D ideal_push_direction = { 0,2,1 };
  };

  class Coast : public SpiderState {
  public:

    Coast(SpiderPig& pig, P3D expected_apogee_) : SpiderState(pig), expected_apogee(expected_apogee_) {
      name = "Coasting";
    }

  private:

    double remaining_tof() {
      P3D p = pig.robot.computeCOM();
      V3D v = pig.robot.computeCOMVelocity();

      double t_rise = v.y() / 9.81;
      double max_height = p.y + v.y() * t_rise * 0.5;
      double t_fall = sqrt(2 * (max_height-0.05) / 9.81);

      
        return t_rise + t_fall;
    }

    void enter_local() override {
      expected_t = remaining_tof();

      V3D ang_velocity = pig.rb_state.angularVelocity;
      V3D local_ang_velocity = pig.rb_state.getLocalCoordinates(ang_velocity);


      completed_angle = local_ang_velocity.x() * 0.01;

      expected_total_flips = abs((completed_angle + local_ang_velocity.x() * expected_t) / ( 2 * PI));
      planned_flips = ceil(expected_total_flips);
      if (expected_total_flips < 0.2)
        planned_flips = 0;
      Logger::consolePrint("%d: Expect to spend %f time in flight, flipping %f times.", pig.n_states, expected_t, expected_total_flips);
    }

    void process_local() override {
      // get remaining time of flight
      double remaining_t = remaining_tof();
      // use current rotation to estimate total angle
      V3D ang_velocity = pig.rb_state.angularVelocity;
      V3D local_ang_velocity = pig.rb_state.getLocalCoordinates(ang_velocity);
      // how many flips?
      double current_total_flips = abs((completed_angle + local_ang_velocity.x() * remaining_t) / (2 * PI));

      if (planned_flips > 0) {
        double factor = current_total_flips / planned_flips;

        // scale z axis 
        double time_dependent = timer.get_dt() * 10;
        triangle.z_scale *= time_dependent * factor + (1-time_dependent);
        triangle.x_scale /= time_dependent * factor + (1 - time_dependent);

      }
      else {
        triangle.z_scale = 1.5;
      }
      if (triangle.z_scale< 0.4) {
        triangle.z_scale = 0.4;
      }
      if (triangle.x_scale > 2) {
        triangle.x_scale = 2;
      }
      triangle.time_step_auto(Vector3d{ 0,0,0 }, 0, timer.get_dt(), 0.2);

      // lerp triangles and get positions
      pos_A = triangle.getAPositions();
      pos_B = triangle.getBPositions();
            
      P3D p = pig.robot.computeCOM();
      V3D v = pig.robot.computeCOMVelocity();

      if (rising && v.y() < 0) {
        rising = false;
        Logger::consolePrint("%d: Reached %f, expected %f", pig.n_states, p.y, expected_apogee.y);
      }

      if (!rising && remaining_t < 0.1)
        pig.to_next();


      completed_angle += local_ang_velocity.x() * timer.get_dt();
    }

    bool rising = true;
    const P3D expected_apogee;
    double expected_t = 0;
    double expected_total_flips = 0;
    double planned_flips;
    double completed_angle;
  };

  class Land : public SpiderState {
  public:

    Land(SpiderPig& pig) : SpiderState(pig) {
      name = "Landing";
    }

  private:
    void process_local() override {

      triangle.fade_scale(10, timer.get_dt());
      triangle.time_step_auto(Vector3d{ 0,0,0 }, 0, timer.get_dt(), 0.2);

      // lerp triangles and get positions
      pos_A = triangle.getAPositions();
      pos_B = triangle.getBPositions();

      P3D p = pig.robot.computeCOM();
      V3D v = pig.robot.computeCOMVelocity();

      if (abs(v.y()) < 0.1)
        pig.to_next();
    }

    void exit_local() override {
      triangle.z_scale = triangle.x_scale = 1.;
    }
  };


  class flipUp :public SpiderState {
  public:
    flipUp(SpiderPig& pig, double flip_velocity_) : SpiderState(pig), target_velocity(flip_velocity_) {
      name = "Flip up";
    }

    void enter_local() override {
      timer.adjust(target_velocity, 30);
    }

    void process_local() override {

      triangle.time_step_auto(timer.get_dt());

      // lerp triangles and get positions
      pos_A = pig.triangle->getAPositions();
      pos_B = pig.triangle->getBPositions();

      if (current_velocity < target_velocity)
        current_velocity += timer.get_dt() * target_velocity / accel_time;

      current_position += current_velocity * timer.get_dt();

      // adjust front legs: pull them quickly up
      pos_A[0].y() = current_position;
      pos_B[0].y() = current_position;

      //pos_A[2].y() += 100 * elapsed_t;
      //pos_B[2].y() += 100 * elapsed_t;
            
      if (current_position > 0.15)
        pig.to_next();
    }

    void exit_local() override {
      timer.reset();
    }

  private:
    const double target_velocity;
    double current_velocity = 0;
    double accel_time = 0.01;
    double current_position = -0.05;
  };


  class preciseJump : public SpiderState {
  public:
    preciseJump(SpiderPig& pig, Vector3d target_) : SpiderState(pig), target(target_)  {
      name = "Precise Jump";
      
    }

    void enter_local() override {
      if (!(jump_scheduled || rotation_scheduled)) {
        // first phase of the jump, nothing done yet
        rotation_scheduled = true;
        auto rotate_phase = new SpiderPigState::rotateTowards(pig, target, true);
        auto rest_phase = new SpiderPigState::Rest(pig, 0.1);

        child_state = rotate_phase;
        rotate_phase->next_state = rest_phase;
      }
      else if (rotation_scheduled && !jump_scheduled) {
        initialize();
        jump_scheduled = true;

        auto extend_phase = new SpiderPigState::Extend(pig, global_req_velocity.norm());
        auto rest_phase = new SpiderPigState::Rest(pig, 0.01);
        auto coast_phase = new SpiderPigState::Coast(pig, expected_apogee);
        auto land_phase = new SpiderPigState::Land(pig);
        auto walk_phase = new SpiderPigState::moveTowards(pig, target, true, 0.3);

        
        child_state = extend_phase;        
        extend_phase->next_state = rest_phase;
        rest_phase->next_state = coast_phase;
        coast_phase->next_state = land_phase;
        land_phase->next_state = walk_phase;
      }
      else if (rotation_scheduled && jump_scheduled) {
        V3D up = pig.robot.glob_up();
        if (up.y() < 0) {
          auto rest_phase_1 = new SpiderPigState::Rest(pig, 1);
          auto flip_phase = new SpiderPigState::flipUp(pig, 4);
          auto rest_phase_2 = new SpiderPigState::Rest(pig, 1);

          child_state = rest_phase_1;
          rest_phase_1->next_state = flip_phase;
          flip_phase->next_state = rest_phase_2;
        }
        else {
          next_state = nullptr;
        }
        pig.to_next();        
      }
      else {
        assert("Scheduling problem in precise jump, aborting!");
      }
    }

    void initialize() {
      // calculate the trajectory
      V3D position = V3D(pig.robot.computeCOM());

      // we need to know which point is higher:      
      V3D diff;
      
      const bool going_high = position.y() < target.y();
      if (going_high) {
        diff = target - position;        
      }
      else {
        diff = position-target;
      }

      
      Vector2d high_point = to_2D_problem(diff);

      // for the "perfect" parabola, we scale the parabola in the unit square to fit our needs
      // calculate the scaling factor (same as height) based on the higher coordinate      
      assert(2 * high_point.x() > high_point.y());
      double height = high_point.x() * high_point.x() / (2 * high_point.x() - high_point.y());

      // what else we need to know
      double remaining_height;
      double derivative_at_start;

      if (going_high) {
        remaining_height = height;
        derivative_at_start = 2;
      }
      else {
        remaining_height = height - high_point.y();
        derivative_at_start = 2 * (high_point.x() / height) - 2;
      }
      double required_velocity = sqrt(2 * remaining_height * 9.81);
      V3D requested_rel_velocity = V3D(  0,derivative_at_start,1 );
      requested_rel_velocity = requested_rel_velocity * (required_velocity / derivative_at_start);
      
      // don't jump into the ground
      assert(requested_rel_velocity.y() > 0);

      global_req_velocity = pig.rb_state.getWorldCoordinates(requested_rel_velocity);

      // calculate target com 
      double velocity_factor = position.y() / requested_rel_velocity.y();
      V3D global_rel_com = global_req_velocity * velocity_factor;
      global_rel_com.y() = 0;

      requested_global_com = position + global_rel_com; //scale the goal down to the height of the com
      expected_apogee = P3D((position + target) / 2);
      expected_apogee.y = position.y() + remaining_height;
    }

    void process_local() override {
      pig.to_next();
    }    

  private:
    const V3D target;

    V3D global_req_velocity;
    V3D requested_global_com;
    P3D expected_apogee;
    
    bool rotation_scheduled = false;
    bool jump_scheduled = false;
  };


  class goToTarget : public SpiderState {
  public:

    goToTarget(SpiderPig& pig, P3D target_, NodeNetwork& network_) : SpiderState(pig), network(network_), target(target_) {
      name = "Navigating to target point";
    }

  private:
    void enter_local() override {
      P3D p = pig.robot.computeCOM();
      if (V3D(p-target).norm() < 0.01) {
        //we are at the goal, exit
        pig.to_next();
        return;
      }
      else if (!initialized) {
        // first build our path
        initialized = true;
        path = network.getShortestPath(pig.robot.computeCOM(), P3D(target));
        it = path.begin();
      }
      else {
        ++it;
      }      
      if (it < path.end()) {
        node& target_node = network.nodes[*it];
        current_target = V3D(target_node.location);
        if (it != path.begin() && network.getCosts(*it,*(it-1))<0) {
          // this edge we need to jump
          child_state = new SpiderPigState::preciseJump(pig, current_target);
        }
        else {
          // we can walk
          child_state = new SpiderPigState::moveTowards(pig, current_target, true, 0.3);
        }
        pig.to_next();
      }
      else {
        // end of the line, now walk towards target directly
        next_state = new SpiderPigState::moveTowards(pig, target, true, 0.1);
        next_state->parent_state = parent_state;
        parent_state = nullptr;
        pig.to_next();
      }
    }


    void process_local() override {
    }

    void exit_local() override {
    }
    NodeNetwork& network;
    V3D target;
    V3D current_target;
    bool initialized = false;
    std::vector<int> path;
    std::vector<int>::iterator it;
  };

}


#endif // SPIDERSTATE_H
