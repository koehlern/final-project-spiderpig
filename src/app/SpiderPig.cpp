#include "SpiderPig.h"
#include "SpiderState.h"
#include "FootTriangle.h"

SpiderPig::SpiderPig(RBEngine* rbEngine_, Vector3d& input, double& angle_control_input, double& _step_size, double& _step_time, double& _radius, timing& timer_, bool& autopause) : control_input(input), angle_input(angle_control_input),
  step_size(_step_size), 
  step_duration(_step_time), radius(_radius),  timer(timer_), robot(rbEngine_, CRL_DATA_FOLDER"/robots/simple/hex.rbs"), rb_state(robot.root->state), angle_length(robot.jointList.size()), advance_state(false), rbEngine(rbEngine_), enable_autopause(autopause) {
   

  radius = 0.23;

  // default triangles
  triangle = new FootTriangle(*this);

  // start spider pig in idle state
    idle_state = new SpiderPigState::Idle(*this);
    idle_state->enter();
    state = idle_state;
    state->next_state = new SpiderPigState::Rest(*this, 20);
    to_next();
}

void SpiderPig::process()
{ 
  if(advance_state) {
    auto old_state = state;
    state->exit();

    // old_state is there to be deleted, only do it if its not the idle state
    if (old_state->name == "Idle")
      old_state = nullptr;

    // first we go for child_states:
    if (state->child_state != nullptr) {      
      // travel along the chain until one state has no next state, then we can push
      // back to the current state
      auto temp_state = state->child_state;
      while (temp_state->next_state != nullptr)
        temp_state = temp_state->next_state;
      temp_state->parent_state = state;
      state = state->child_state;      
    }
    // then we go after the parents
    else if (state->parent_state != nullptr) {
      assert(state->next_state == nullptr && "Found a state with both parent and next state, that is illegal");
      state = state->parent_state;
      delete state->child_state;
      state->child_state = nullptr;
    }
    else if (state->next_state == nullptr) {
      state = idle_state;
    }
    else {
      // we want to move to the next state, and it exists. we can also
      // delete the current state
      state = state->next_state;
    }
    advance_state = false;
    state->enter();

    if (enable_autopause) {
      schedule_pause = true;
      return;
    }
    
    // show state change in the console
    // Logger::consolePrint("%d: Changed state to:  %s ", n_states, state->name.c_str());
  }

  // process in the current state
  state->process();
  universal_time += timer.get_dt();
}

void SpiderPig::get_current_angles(Eigen::VectorXd& angles)
{
  assert(robot.jointList.size() == angles.size());
  for (int i = 0; i < robot.jointList.size(); ++i)
    angles[i] = robot.jointList[i]->getCurrentJointAngle();
}

void SpiderPig::get_default_angles(Eigen::VectorXd& angles)
{
  assert(robot.jointList.size() == angles.size());
  for (int i = 0; i < robot.jointList.size(); ++i)
    angles[i] = robot.jointList[i]->defaultJointAngle;  
}

void SpiderPig::set_current_angles(Eigen::VectorXd& angles)
{
  assert(robot.jointList.size() == angles.size());

  for (int i = 0; i < robot.jointList.size(); ++i)
    robot.jointList[i]->desiredControlSignal = angles[i];  
}

void SpiderPig::set_specific_angle(size_t index, double angle)
{
  while (angle > PI)
    angle -= 2*PI;
  while (angle < -PI)
    angle += 2*PI;
  robot.jointList[index]->desiredControlSignal = angle;
}

void SpiderPig::set_specific_angles(size_t count, size_t* index, double* angle)
{
  for (int i = 0; i<count; i++) {
    set_specific_angle(index[i], angle[i]);
  }
}

Eigen::Vector3d SpiderPig::IK_angles_rel(size_t leg, P3D goal)
{  
  return IK_angles_abs(leg, getWorldCoordinates(goal));
}

Eigen::Vector3d SpiderPig::IK_angles_abs(size_t leg, P3D goal)
{
  // get involved joints
  RBJoint* top_joint = robot.jointList[i_legs[leg]];
  RBJoint* mid_joint = robot.jointList[i_legs[leg]+i_joints[1]];
  RBJoint* end_joint = robot.jointList[i_legs[leg]+i_joints[2]];

  // calculate direction to goal from leg top joint
  V3D direction_from_top_joint = V3D(top_joint->getWorldPosition(), goal);

  // depending on side of robot, we need to change the calculation
  double side = 1;
  if (leg == 0 || leg == 2 || leg == 4)
    side = -1;

  // first angle is just to point into direction
  double top_angle = -planeAngleBetween(side*robot.glob_right(), direction_from_top_joint, robot.glob_up());
  //if (top_angle < -PI)
    //top_angle += 2 * PI;
  
  // remaining problem is only "two" dimensional
  // where will mid_joint lie?
  V3D top_dir = projOnPlane(direction_from_top_joint, robot.glob_up());
  top_dir.normalize();

  // what is the relative angle to the target compared to our frame of reference
  V3D direction_from_mid_joint = V3D(goal) - (V3D(top_joint->getWorldPosition())+(top_dir * top_l));
  double distance = direction_from_mid_joint.norm();
  double max_dist = mid_l + end_l-0.001;
  double min_dist = end_l - mid_l+0.001;
  boundToRange(distance, min_dist, max_dist);
  V3D rotation_axis = robot.glob_up().cross(top_dir);
  double dir_angle = planeAngleBetween(top_dir, direction_from_mid_joint, rotation_axis);

  // calculate joint distance (along the direction)
  double y = (mid_l * mid_l - end_l * end_l + distance * distance) / (2 * distance);
  double alpha = safeACOS(y / mid_l);
  double beta = safeACOS((distance-y) / end_l);

  return Eigen::Vector3d(top_angle, -side * (-dir_angle+alpha),side *(alpha+beta));
}

void SpiderPig::IK_set_foot_lcl(size_t leg, V3D goal)
{
  // get angles
  auto angles = IK_angles_rel(leg, getP3D(goal));

  // set angles
  for (int i = 0; i < 3; i++) {
    set_specific_angle(i_joints[i] + i_legs[leg], angles[i]);
  }
}

void SpiderPig::to_idle()
{
  purge_states();
}

void SpiderPig::to_pause() {
  purge_states();
  state->next_state = new SpiderPigState::Pause(*this);
  top_state = state->next_state;
  advance_state = true;
}

void SpiderPig::to_default(double trans_time)
{
  purge_states();
  state->next_state = new SpiderPigState::directControl(*this, 1);
  state->next_state->name = "Lerping to default";
  top_state = state->next_state;
  advance_state = true;
}

void SpiderPig::to_oneLegIK(Vector3d& goal, size_t leg)
{
  purge_states();
  state->next_state = new SpiderPigState::OneLegIK(*this, goal, leg);
  top_state = state->next_state;
  advance_state = true;
}

void SpiderPig::to_directControl()
{
  purge_states();
  state->next_state = new SpiderPigState::directControl(*this, control_input, angle_input);
  top_state = state->next_state;
  advance_state = true;
}

void SpiderPig::to_jump(double velocity) {
  if (state->name != "Jump") {
    state->child_state = new SpiderPigState::jump(*this, velocity);
    advance_state = true;
  }
}

void SpiderPig::to_precise_jump(Vector3d& goal) {
  if (state->name != "Precise Jump") {
    state->child_state = new SpiderPigState::preciseJump(*this, goal);
    advance_state = true;
  }
}

void SpiderPig::to_move_towards(Vector3d& goal)
{
  purge_states();
  state->next_state = new SpiderPigState::moveTowards(*this, goal);
  top_state = state->next_state;
  advance_state = true;
}

void SpiderPig::to_walk_to_target(P3D goal, NodeNetwork& network_) {
  state->child_state = new SpiderPigState::goToTarget(*this, goal, network_);
  advance_state = true;
}

void SpiderPig::to_rotate_towards(Vector3d& goal)
{
  purge_states();
  state->next_state = new SpiderPigState::rotateTowards(*this, goal);
  top_state = state->next_state;
  advance_state = true;
}

void SpiderPig::to_flip(double flip_velocity)
{
    state->child_state = new SpiderPigState::flipUp(*this, flip_velocity);
    advance_state = true;  
}

void SpiderPig::purge_states()
{
  state->exit();
  delete top_state;
  top_state = nullptr;
  state = idle_state;
  state->enter();

  assert(state != nullptr && "ERROR: SpiderPig state is nullptr!");
  assert(state->name == "Idle" && "ERROR: state after a purge should be Idle");
}

void SpiderPig::to_next()
{
  advance_state = true;
}

