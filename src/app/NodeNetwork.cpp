#include "NodeNetwork.h"
#include "utils/ray.h"
#include "gui/renderer.h"
#include <set>



void NodeNetwork::load()
{
  std::ifstream file(filename, std::ios::in);

  // first we read the size:
  std::string word;
  file >> word;
  assert(word == "size:");
  double size;
  file >> size;

  // then all the positions;
  file >> word;
  assert(word == "positions:");
  for (int i=0; i< size; i++) {
    P3D position;
    file >> position.x;
    file >> position.y;
    file >> position.z;
    nodes.push_back(node(i,position));
  }

  // then the cost matrix
  file >> word;
  assert(word == "cost:");
  costs.resize(size, size);
  for (int i=0; i< size; i++) {
    for (int j=0; j< size; j++) {
      file >> costs(i, j);
    }
  }
  file.close();
}

void NodeNetwork::save() {
  std::ofstream file(filename, std::ios::out);
  file.clear();

  file << "size: " << nodes.size() << "\n";
  
  file << "positions:\n";
  for (int i = 0; i< nodes.size(); i++) {
    file << nodes[i].location.x << " " << nodes[i].location.y << " " << nodes[i].location.z << "\n";
  }

  file << "cost:\n";
  file << costs;
  file.close();
}

void NodeNetwork::hover(Ray& mouseRay) {
  Plane ground = { P3D(0,0,0),V3D(0,1,0) };
  mouseRay.getDistanceToPlane(ground, &mouseLocation);

  // check if we are hovering any node and display information if we do
  for (size_t i = 0; i < nodes.size(); i++) {
    if (mouseRay.getDistanceToPoint(nodes[i].location, nullptr) < node_draw_radius) {
      if (hovered_Node != i)
          Logger::consolePrint("Node ID %d: cost until here is %f and heuristic cost is %f - estimated total is therefore %f.\n", i, nodes[i].current_cost, nodes[i].heuristic_cost, nodes[i].current_cost  + nodes[i].heuristic_cost);
      hovered_Node = i;
      return;
    }
  }
  hovered_Node = -1;
}

void NodeNetwork::click()
{
  switch (mode) {
  case editMode::addNodes:
    if (hovered_Node == -1) {
      // no node there, add one
      add(mouseLocation);
    }
    else {
      // we are hovering a node, remove it
      remove(hovered_Node);
      hovered_Node = -1;
    }
    break;
  case editMode::connectNodes:
    if (selected_Node == hovered_Node) {
      // toggle selected node
      selected_Node = -1;
      return;
    }
    
    if (hovered_Node >= 0 && selected_Node>= 0) {
      // two different nodes, "connect" them
      connect(selected_Node, hovered_Node);
    }
    selected_Node = hovered_Node;
    break;    
  }
}

void NodeNetwork::remove(int node) {
  assert(node >= 0 && node < nodes.size());
  int new_size = nodes.size() - 1;

  // erase and renumber:
  nodes.erase(nodes.begin() + node);
  for (int i = 0; i < nodes.size(); i++) {
    nodes[i].id = i;
  }

  Eigen::MatrixXd new_costs(new_size, new_size);

  if (node == 0) {
    new_costs = costs.block(1, 1, new_size, new_size);
  }
  else if (node == nodes.size()) {
    costs.conservativeResize(new_size, new_size);
    return;
  }
  else {
    // copy all the blocks surrounding the removed row and column
    new_costs.block(0, 0, node, node) = costs.block(0, 0, node, node);
    new_costs.block(0, node, node, new_size - node) = costs.block(0, node + 1, node, new_size - node);
    new_costs.block(node, 0, new_size - node, node) = costs.block(node + 1, 0, new_size - node, node);
    new_costs.block(node, node, new_size - node, new_size - node) = costs.block(node + 1, node + 1, new_size - node, new_size - node);
  }
  costs = new_costs;
}

void NodeNetwork::draw(const Shader& rbShader)
{
  // draw all nodes
  for (int i = 0; i < nodes.size(); i++) {
    if (hovered_Node == i) // hovered node is yellow
      drawSphere(nodes[i].location, node_draw_radius * 1.1, rbShader, V3D(1, 1, 0));
    else if (selected_Node == i) // selected node is blue
      drawSphere(nodes[i].location, node_draw_radius * 1.1, rbShader, V3D(0.2, 0, 1));
    else if (i < in_path.size() && in_path[i]) // nodes on the path are pinkish
      drawSphere(nodes[i].location, node_draw_radius * 1.3, rbShader, V3D(1, 0.8, 0.8));
    else if (i < finished_nodes.size() && finished_nodes[i]) // nodes that were explored are white
      drawSphere(nodes[i].location, node_draw_radius * 1.1, rbShader, V3D(.8, .8, .8));
    else if (i < unknown_nodes.size() && unknown_nodes[i])  // noted that we did not consider are black
      drawSphere(nodes[i].location, node_draw_radius * 1.1, rbShader, V3D(.0, .0, .0));
    else  // nodes that are "known" are green
      drawSphere(nodes[i].location, node_draw_radius * 1.1, rbShader, V3D(0.,0.5,0.));

  }

  float edge_rad = node_draw_radius * 0.5;

  // draw all connections
  for (int i = 1; i<nodes.size(); i++) {
    for (int j = 0; j<i; j ++) {
      if (costs(i,j) > 0) {
        if (i < in_path.size() && in_path[i] && in_path[j])
          drawCylinder(nodes[i].location, nodes[j].location, edge_rad * 2, rbShader, V3D(1, 1, 1));
        else
          drawCylinder(nodes[i].location, nodes[j].location, edge_rad, rbShader, V3D(.5, .5, .5));
      }
      if (costs(i, j) < 0) {
        if (i < in_path.size() && in_path[i] && in_path[j])
          drawCylinder(nodes[i].location, nodes[j].location, edge_rad * 2, rbShader, V3D(0.5, 0.5, 1));
        else
          drawCylinder(nodes[i].location, nodes[j].location, edge_rad, rbShader, V3D(.3, .3, .7));
      }
    }
  }
}

void NodeNetwork::add(P3D& location)
{
  nodes.push_back(node(nodes.size(), mouseLocation));
  hovered_Node = nodes.size() - 1;

  // add space to the matrix and initialize them with 0
  costs.conservativeResize(nodes.size(), nodes.size());
  costs.row(nodes.size() - 1).setZero();
  costs.col(nodes.size() - 1).setZero();
}

void NodeNetwork::connect(int node_a, int node_b, double cost)
{
  assert(node_a != node_b);

  // only half of the matrix gets used
  if (node_a < node_b)
    std::swap(node_a, node_b);

  // and edge between two nodes goes trough 3 states:
  // 0: no edge
  // > 0: normal edge
  // < 0: jump edge

  // if we already have an edge, make it to a jump edge (negate the cost)
  if (costs(node_a, node_b) > 0) {
    costs(node_a, node_b) = - 2* costs(node_a, node_b);
    return;
  }
  if (costs(node_a, node_b) < 0) { //remove the edge
    costs(node_a, node_b) = 0;
    return;
  }
  if (cost == 0) {
    V3D distance = V3D(nodes[node_a].location - nodes[node_b].location);
    cost = distance.norm();
  }

  // connect nodes
  costs(node_a, node_b) = cost;
}

void NodeNetwork::explore_node(node& n) {
  // collect neighbors of node and add them to the queue
  for (int i=0; i<nodes.size(); i++) {
    if (i != n.id) {
      double edge_cost = abs(getCosts(n.id, i));
      // is there an edge?
      if (edge_cost > 0) {
        // is it a new node?
        if (unknown_nodes[i]) {
          unknown_nodes[i] = false;
          known_nodes.emplace(updateNode(n, i));
        }
        else if (!finished_nodes[i]){
          // we know the node already, but it is not finished. update if we are cheaper
          node& i_node = nodes[i];
          double path_cost = n.current_cost + edge_cost;
          if (path_cost < i_node.current_cost) {
            // erasing, updating and emplace again
            known_nodes.erase(known_nodes.find(i_node));
            known_nodes.emplace(updateNode(n, i));
          }
        }
      }    
    }
  }
  finished_nodes[n.id] = true;
}

// find and update the specified node with the new distance
node NodeNetwork::updateNode(node& parent, int id_)
{
  node& ret = nodes[id_];
  ret.current_cost = parent.current_cost + abs(getCosts(parent.id, id_));
  ret.heuristic_cost = V3D(network_goal - ret.location).norm();
  ret.best_predecessor = parent.id;
  return ret;
}


double NodeNetwork::getCosts(int node_a, int node_b) {
  assert(node_a != node_b);

  // make sure we acces the correct part of the cost matrix
  if (node_a < node_b)
    std::swap(node_a, node_b);  
  
  return costs(node_a, node_b);  
}

std::vector<int>& NodeNetwork::getShortestPath(const P3D& start, const P3D& end) {
  path.clear();
  goal = end;

  // first find the closest nodes to start and end:
  int n_start = 0, n_end = 0;
  double d_start = std::numeric_limits<double>::max(), d_end = std::numeric_limits<double>::max();
  for (int i = 0; i < nodes.size(); i++) {
    double temp_start = V3D(nodes[i].location - start).norm();
    if (temp_start < d_start) {
      d_start = temp_start;
      n_start = i;
    }
    double temp_end = V3D(nodes[i].location - goal).norm();
    if (temp_end < d_end) {
      d_end = temp_end;
      n_end = i;
    }
  }

  network_goal = nodes[n_end].location;

  // so we use A*
  node& start_node = nodes[n_start];
  start_node.current_cost = 0;
  start_node.heuristic_cost = V3D(start_node.location - end).norm();

  // house keeping
  known_nodes.clear();
  known_nodes.insert(start_node);
  unknown_nodes = std::vector<bool>(nodes.size(), true);
  finished_nodes = std::vector<bool>(nodes.size(), false);
  in_path = std::vector<bool>(nodes.size(), false);
  unknown_nodes[n_start] = false;

  // explore the first known node (lowest cost + heuristic) and consume it
  node current_node = (*known_nodes.begin());
  while (current_node.id != n_end) {
    known_nodes.erase(known_nodes.begin());
    explore_node(current_node);
    if (known_nodes.size() == 0) {// node is not connected, end A*
      current_node = nodes[n_start];
      break;
    }
    current_node = *known_nodes.begin();
  }
    
  // we finished A* and can return the path:
  while (current_node.id != n_start) {
    int id = current_node.id;
    path.push_back(id);
    in_path[id] = true;
    current_node = nodes[current_node.best_predecessor];
  }
  path.push_back(n_start);
  in_path[n_start] = true;
  std::reverse(path.begin(), path.end());
  return path;  
}

