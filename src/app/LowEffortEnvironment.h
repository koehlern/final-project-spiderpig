#pragma once
#include "utils/mathUtils.h"
#include "gui/shader.h"
#include <random>
#include "gui/renderer.h"

class LowEffortEnvironment
{
public:
  LowEffortEnvironment(P3D low_i, P3D high_i) : x_max(high_i.x), x_min(low_i.x), z_max(high_i.z), z_min(low_i.z) {}

  const double x_max, x_min, z_max, z_min;
  int object_count = 200;
  int random_seed = 17;

  void draw(const Shader& rbShader) {
    std::mt19937 rng;
    rng.seed(random_seed);
    std::uniform_real_distribution<double> distribution_x(x_min, x_max);
    std::uniform_real_distribution<double> distribution_dim(0.5, 2);
    std::uniform_real_distribution<double> distribution_z(z_min, z_max);
    std::uniform_int_distribution<int> model_distribution(0, 3);


    V3D color(0.8, 0.8, 0.8);

    for (int i = 0; i<object_count; i++) {
      int model = model_distribution(rng);
      double x = distribution_x(rng);
      double z = distribution_z(rng);


      P3D low(x, 0, z);
      P3D high(x, 1, z);
      V3D point(0, distribution_dim(rng), 0);

      double rad = 0.5;

      V3D dims(distribution_dim(rng), distribution_dim(rng), distribution_dim(rng));

      switch (model) {
      case 0:
        drawSphere(low, rad, rbShader, color);
        break;
      case 1:
        drawCylinder(low, high, rad, rbShader, color);
        break;
      case 2:
        drawCone(low, point, rad, rbShader, color);
        break;
      case 3:
        drawCuboid(low, Quaternion::Identity(), dims, rbShader, color);
        break;
      }
    }
  }
};

class LowEffortWall {
public:
  LowEffortWall(P3D start_, P3D end_) : start(start_), end(end_){}

  void draw(const Shader& rbShader) {
    double distance = V3D(start - end).norm();
    V3D path = V3D(end-start).normalized();
    for (double k = 0; k < distance; k += 0.2) {
      drawCone(start + (path * k), V3D(0, 1, 0), 0.2, rbShader, V3D(0.5, 0.5, 0.5));
    }
  }

  P3D start, end;
};

