#pragma once


#include <gui/application.h>
#include <gui/shader.h>
#include <gui/renderer.h>
#include <gui/light.h>
#include <gui/camera.h>
#include <gui/ImGuizmo.h>
#include <utils/logger.h>
#include <robot/Robot.h>
#include <ode/ODERBEngine.h>

#include "SpiderPig.h"
#include "FootTriangle.h"
#include "../../build/src/app/timing.h"
#include "NodeNetwork.h"
#include "LowEffortEnvironment.h"


/**
    Space: run/pause sim
    Click on robot: highlight the body part, print out the name.
*/

class App : public Basic3DAppWithShadows {
public:
  App(const char* title = "CRL Playground - Creature Locomotion app",
      std::string iconPath = CRL_DATA_FOLDER"/icons/icon.png")
    : Basic3DAppWithShadows(title, iconPath) {

    camera = TrackingCamera(2);
    camera.aspectRatio = float(width) / height;
    camera.rotAboutUpAxis = -0.0;
    camera.rotAboutRightAxis = 0.5;

    light.s = 0.03f;
    shadowbias = 0.0f;

    glEnable(GL_DEPTH_TEST);

    showConsole = true;
    automanageConsole = true;
    Logger::maxConsoleLineCount = 10;
    consoleHeight = 225;

    odeRbEngine->loadRBsFromFile(CRL_DATA_FOLDER "/environment/Ground.rbs");

    robot.showMeshes = false;
    robot.showSkeleton = true;

    robot.setRootState(P3D(0, 0.5, 0), Quaternion::Identity());

    // set all the joints to position mode
    for (int i = 0; i < robot.getJointCount(); i++) {
      robot.getJoint(i)->controlMode = RBJointControlMode::POSITION_MODE;
    }

    // initialize joint data in app:
    jointAngles.resize(robot.jointList.size());

    this->targetFramerate = 30;
    this->limitFramerate = true;

    for (int i = 0; i < MY_PLOT_N; i++)
      myPlotValues[i] = 0;
  }

  virtual ~App() override { }

  virtual void resizeWindow(int width, int height) override {
    camera.aspectRatio = float(width) / height;
    return Application::resizeWindow(width, height);
  }

  bool mouseMove(double xpos, double ypos) override {
    P3D rayOrigin;
    V3D rayDirection;
    camera.getRayFromScreenCoordinates(xpos, ypos, rayOrigin, rayDirection);
    Ray mouseRay(rayOrigin, rayDirection);

    if (network.mode == editMode::disable) {
      if (!mouseState.dragging) {
        // this bit of code will check for mouse-ray robot intersections all the time.
        if (selectedRB != NULL)
          selectedRB->rbProps.selected = false;

        P3D selectedPoint;

        selectedRB = robot.getFirstRBHitByRay(mouseRay, selectedPoint, false, true);

        if (selectedRB != NULL) { selectedRB->rbProps.selected = true; }
      }
      else {}
    }
    else {
      network.hover(mouseRay);
    }

    camera.processMouseMove(mouseState, keyboardState);
    return true;
  }

  bool mouseButtonReleased(int button, int mods) override {
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
      if (network.mode != editMode::disable) {
        // we are in a network editing mode, first react to clicks:
        if (!mouseState.dragging)
          network.click();
      }
      else if (selectedRB) {
        selectedRB->rbProps.selected = false;
        selectedRB = nullptr;
      }
    }
    return true;
  }

  bool mouseButtonPressed(int button, int mods) override {
    if (button == GLFW_MOUSE_BUTTON_LEFT || button == GLFW_MOUSE_BUTTON_RIGHT) {
      if (selectedRB != nullptr) {
        if (selectedRB->pJoint == nullptr)
          Logger::consolePrint("Clicked on BodyLink %s (root)\n", selectedRB->name.c_str());
        else
          Logger::consolePrint("Clicked on BodyLink %s (parent joint is %s with id %d))\n", selectedRB->name.c_str(),
                               selectedRB->pJoint->name.c_str(), selectedRB->pJoint->jIndex);
      }
    }

    return true;
  }

  bool scrollWheel(double xoffset, double yoffset) override {
    camera.processMouseScroll(xoffset, yoffset);
    return true;
  }

  void computeAndApplyControlInputs(double dt) {
    simTime += dt;

    
  }

  void process() override {
    
    
    if (!appIsRunning)
      return;

    if (auto_update_path)
      network.getShortestPath(robot.computeCOM(), P3D(guizmoPosition));

    // handle axis input from gamepad
    if (glfwJoystickIsGamepad(GLFW_JOYSTICK_1)) {
      GLFWgamepadstate state;
      if (glfwGetGamepadState(GLFW_JOYSTICK_1, &state))
      {
        control_input[0] = -state.axes[GLFW_GAMEPAD_AXIS_RIGHT_X];
        control_input[2] = -state.axes[GLFW_GAMEPAD_AXIS_LEFT_Y];
        angle_input = state.axes[GLFW_GAMEPAD_AXIS_LEFT_X];

        // ignore small values
        if (abs(control_input[0]) < 0.2)
          control_input[0] = 0;
        if (abs(control_input[2]) < 0.2)
          control_input[2] = 0;
        if (abs(angle_input) < 0.25)
          angle_input = 0;

        if (control_input.norm() > 1)
          control_input.normalize();

        // only on push, not all the time
        if (!button_A_toggle) {
          if (state.buttons[GLFW_GAMEPAD_BUTTON_A])
          {
            piggy.to_jump(jump_velocity);
            button_A_toggle = true;
          }
        }
        else {
          if (!state.buttons[GLFW_GAMEPAD_BUTTON_A])
          {            
            button_A_toggle = false;
          }
        }

        if (!button_START_toggle) {
          if (state.buttons[GLFW_GAMEPAD_BUTTON_START])
          {
            piggy.to_directControl();
            button_START_toggle = true;
          }
        }
        else {
          if (!state.buttons[GLFW_GAMEPAD_BUTTON_START])
          {
            button_START_toggle = false;
          }
        }

        if (!button_BACK_toggle) {
          if (state.buttons[GLFW_GAMEPAD_BUTTON_BACK])
          {
            piggy.to_flip(flip_velocity);
            button_BACK_toggle = true;
          }
        }
        else {
          if (!state.buttons[GLFW_GAMEPAD_BUTTON_BACK])
          {
            button_BACK_toggle = false;
          }
        }

        if (!button_Y_toggle) {
          if (state.buttons[GLFW_GAMEPAD_BUTTON_Y])
          {
            piggy.to_precise_jump(guizmoPosition);
            button_Y_toggle = true;
          }
        }
        else {
          if (!state.buttons[GLFW_GAMEPAD_BUTTON_Y])
          {
            button_Y_toggle = false;
          }
        }        
        
        /*
        input_speed(state.axes[GLFW_GAMEPAD_AXIS_RIGHT_TRIGGER]);
        */
      }
    }

    

    
    if (timer.slowMotion) {
      
      piggy.process();
      if (piggy.schedule_pause && debug_autopause) {
        appIsRunning = false;
        piggy.schedule_pause = false;
        return;
      }
      auto dt = timer.get_dt();
      odeRbEngine->step(dt);

      
    }
    else {
      // we need to do enough work here until the simulation time is caught up
      // with the display time...
      double tmpT = 0;
      while (tmpT < 1.0 / targetFramerate) {
        
        piggy.process();
        auto dt = timer.get_dt();
        odeRbEngine->step(dt);

        tmpT += dt;

               
      }
    }

    light.target.x() = robot.root->state.pos.x;
    light.target.z() = robot.root->state.pos.z;

    camera.target.x = robot.root->state.pos.x;
    camera.target.z = robot.root->state.pos.z;
  }

  virtual void drawAuxiliaryInfo() {
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    drawFPS();

    drawConsole();

    drawImGui();

    ImGui::EndFrame();
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
  }

  

  // objects drawn with a shadowMapRenderer (during shadow pass) will cast a shadow
  virtual void drawShadowCastingObjects() override {
    robot.draw(shadowMapRenderer);

    

  }

  // objects drawn with a shadowShader (during the render pass) will have shadows cast on them
  virtual void drawObjectsWithShadows() override {
    ground.draw(shadowShader, V3D(0.6, 0.6, 0.8));
  }

  // objects drawn with basic shadowShader (during the render pass) will not have shadows cast on them
  virtual void drawObjectsWithoutShadows() override {
    robot.draw(basicShader);
    if (show_network)
      network.draw(basicShader);

    if (show_env) {
      env.draw(basicShader);
      wall.draw(basicShader);
    }
    
  }

  virtual bool keyPressed(int key, int mods) override {
    if (key == GLFW_KEY_SPACE) {
      appIsRunning = !appIsRunning;
      return true;
    }
    return false;
  }

  // draws the UI using ImGui. you can add your own UI elements (see below)
  virtual void drawImGui() {

    using namespace ImGui;

    SetNextWindowPos(ImVec2(0, 0), ImGuiCond_Once);
    Begin("Main Menu");

    Text("Play:");
    SameLine();
    ToggleButton("Play App", &appIsRunning);

    const ImVec2 button_size = ImVec2(300, 30);

    if (!appIsRunning) {
      SameLine();
      if (ArrowButton("tmp", ImGuiDir_Right)) {
        appIsRunning = true;
        process();
        appIsRunning = false;
      }
    }

    if (TreeNode("Draw options...")) {
      Checkbox("Draw Console", &showConsole);
      Checkbox("Draw Meshes", &robot.showMeshes);
      Checkbox("Draw Skeleton", &robot.showSkeleton);
      Checkbox("Draw Joint Axes", &robot.showJointAxes);
      Checkbox("Draw Joint Limits", &robot.showJointLimits);
      Checkbox("Draw Collision Primitives", &robot.showCollisionSpheres);
      Checkbox("Draw MOI box", &robot.showMOI);
      Checkbox("Draw Navigation Network", &show_network);
      Checkbox("Draw Environment", &show_env); 
      {double min = 0.001; double max = 1;
      SliderFloat("Network Scale", &network.node_draw_radius, min, max); }

      TreePop();
    }

    if (CollapsingHeader("Timing")) {
      Indent();
      Checkbox("Debug Autopause", &debug_autopause);
      Checkbox("Enable Slow Motion", &timer.slowMotion);
      if (timer.slowMotion) {
        double min = 4, max = 100;
        SliderScalar("Factor", ImGuiDataType_Double, &timer.slowMotion_ratio, &min, &max);
      }

      Unindent();
    }

    // Network stuff
    if (CollapsingHeader("Network")) {
      Indent();
      const char* items[5] = { "Disable", "Edit Nodes", "Edit Edges" };
      Combo("Network Edit Mode", (int*)&network.mode, items, 3);
      
      if (Button("Save Network", button_size)) {
        network.save();
      }

      if (Button("Create Path to Guizmo", button_size))
        network.getShortestPath(robot.computeCOM(), P3D(guizmoPosition));

      Checkbox("Auto Update Path", &auto_update_path);


      Unindent();
    }

    // Phases
    if (CollapsingHeader("Phases")) {
      Indent();

      if (Button("Set Default", button_size))
        piggy.to_default(2);

      if (Button("Pause", button_size))
        piggy.to_pause();

      InputInt("Leg (0-5) for IK", &selected_leg);
      if (Button("Activate 1 leg IK", button_size))
        piggy.to_oneLegIK(guizmoPosition, selected_leg);

      if (Button("Start Walking by Gamepad", button_size))
        piggy.to_directControl();

      if (Button("Rotate Towards", button_size))
        piggy.to_rotate_towards(guizmoPosition);

      if (Button("Move Towards", button_size))
        piggy.to_move_towards(guizmoPosition);

      if (Button("Walk trough network to target", button_size))
        piggy.to_walk_to_target(P3D(guizmoPosition), network);


      Unindent();
    }

    if (CollapsingHeader("Triangle Parameters")) {
      {
        double r_min = 0.1; double r_max = 0.3;
        SliderScalar("Radius", ImGuiDataType_Double, &spider_radius, &r_min, &r_max);
      }

      {
        double t_min = 0; double t_max = 0.3;
        SliderScalar("Stepsize", ImGuiDataType_Double, &step_size, &t_min, &t_max);
      }

      {
        double t_min = 0; double t_max = 1;
        SliderScalarN("Relative Position", ImGuiDataType_Double, piggy.triangle->rel_position.data(), piggy.triangle->rel_position.size(),&t_min, &t_max);
      }

      {
        double t_min = -1; double t_max = 1;
        SliderScalar("Relative Angle", ImGuiDataType_Double, &piggy.triangle->rel_angle, &t_min, &t_max);
      }

      {
        double t_min = -0.1; double t_max = .1;
        SliderScalarN("global offset", ImGuiDataType_Double, piggy.triangle->glob_offset.data(), control_input.size(), &t_min, &t_max);
      }      
      
      {
        double t_min = -PI; double t_max = PI;
        SliderScalar("triangle rotation front", ImGuiDataType_Double, &piggy.triangle->front_rotation, &t_min, &t_max);
      }

      {
        double t_min = 0.1; double t_max = 2;
        SliderScalar("x_scale", ImGuiDataType_Double, &piggy.triangle->x_scale, &t_min, &t_max);
      }
      {
        double t_min = 0.1; double t_max = 2;
        SliderScalar("z_scale", ImGuiDataType_Double, &piggy.triangle->z_scale, &t_min, &t_max);
      }
      {
        double t_min = 0.5; double t_max = 50;
        SliderScalar("Jump Velocity", ImGuiDataType_Double, &jump_velocity, &t_min, &t_max);
      }
      {
        double t_min = 0; double t_max = 5;
        SliderScalar("Flip Velocity", ImGuiDataType_Double, &flip_velocity, &t_min, &t_max);
      }

      {
        double t_min = 0.05; double t_max = 2;
        SliderScalar("Transition Time", ImGuiDataType_Double, &transition_time, &t_min, &t_max);
      }
      
      Unindent();
    }

    if (CollapsingHeader("Control Input Diagnostics")) {
      double min = -PI, max = PI;

      InputScalarN("Control Input", ImGuiDataType_Double, control_input.data(), control_input.size());
      SliderScalar("Input angle", ImGuiDataType_Double, &angle_input, &min, &max);

      Unindent();
    }

    // Direct Joint Angles
    if (CollapsingHeader("Diagnostics & Joint Angles", true)) {
      Indent();
      double min = -PI, max = PI;
      for (auto& joint : robot.jointList) {
        jointAngles[joint->jIndex] = joint->getCurrentJointAngle();
        const char* text = joint->name.c_str();
        if (SliderScalar(text, ImGuiDataType_Double, &jointAngles[joint->jIndex], &min, &max))
          joint->desiredControlSignal = jointAngles[joint->jIndex];
      }

      Unindent();
    }

    if (CollapsingHeader("Environment")) {
      Indent();
      {
        size_t min = 0; size_t max = 500;
        SliderInt("Random Object Count", &env.object_count, min, max);
      }
      {
        size_t min = 0; size_t max = 20;
        SliderInt("Random Seed", &env.random_seed, min, max);
      }

      Unindent();
    }

    // add your own UI elements for example like this:
    if (CollapsingHeader("Diagnostics", true)) {
      Indent();
      
      InputScalarN("velocity vector", ImGuiDataType_Double, piggy.triangle->rel_velocity.data(), piggy.triangle->rel_velocity.size());

      myDouble = piggy.triangle->rel_velocity.norm();

      InputDouble("velocity norm", &myDouble);

      // make a plot
      PlotLines("velocity norm", myPlotValues, MY_PLOT_N, myPlotCounter, NULL, FLT_MAX, FLT_MAX, {0, 200});
      // You probably would want to put the following lines in the process() function.
      // I put it here so it's all in one place.
      myPlotValues[myPlotCounter] = piggy.triangle->rel_velocity.norm();

      PlotLines("rel_rot", rel_AnglePlotValues, MY_PLOT_N, myPlotCounter, NULL, FLT_MAX, FLT_MAX, { 0, 200 });
      // You probably would want to put the following lines in the process() function.
      // I put it here so it's all in one place.
      rel_AnglePlotValues[myPlotCounter] = piggy.triangle->rel_angle;

      PlotLines("ang_vel", vel_AnglePlotValues, MY_PLOT_N, myPlotCounter, NULL, FLT_MAX, FLT_MAX, { 0, 200 });
      // You probably would want to put the following lines in the process() function.
      // I put it here so it's all in one place.
      vel_AnglePlotValues[myPlotCounter] = piggy.triangle->rot_velocity;



      myPlotCounter = (myPlotCounter + 1) % MY_PLOT_N;
      Text("guizmo:");
      Checkbox("enabled", &guizmoEnabled);
      InputScalarN("position", ImGuiDataType_Double, guizmoPosition.data(), 3);

      Unindent();
    }

    // example code on how to use the ImGuizmo
    if (guizmoEnabled) {
      ImGuizmo::BeginFrame();
      ImGuiIO& io = ImGui::GetIO();
      ImGuizmo::SetRect(0, 0, io.DisplaySize.x, io.DisplaySize.y);

      // we use it in translate mode and need to provide the corresponding
      // transformation matrix to ImGuizmo ...
      auto transform = glm::translate(glm::mat4(1.f), toGLM(guizmoPosition));
      ImGuizmo::Manipulate(glm::value_ptr(camera.getViewMatrix()), glm::value_ptr(camera.getProjectionMatrix()),
                           ImGuizmo::TRANSLATE, ImGuizmo::WORLD, glm::value_ptr(transform));

      // ... and thus after manipulation, we extract the changed position
      // from the transformation matrix.
      guizmoPosition = Vector3d(
        transform[3][0],
        transform[3][1],
        transform[3][2]
      );
    }

    ImGui::End();
  }

  virtual bool drop(int count, const char** fileNames) override { return true; }

public:
  SimpleGroundModel ground; // model to draw the ground

  // the simulation engine
  crl::sim::ODERBEngine* odeRbEngine = new crl::sim::ODERBEngine();

  // Robot Wrapper
  SpiderPig piggy = SpiderPig(odeRbEngine, control_input, angle_input, step_size, transition_time, spider_radius, timer, debug_autopause);

  // the robot to load. uncomment/comment to change robot model
  Robot& robot = piggy.robot;
   // Robot(odeRbEngine, CRL_DATA_FOLDER"/robots/simple/hex.rbs");
  //      Robot(odeRbEngine, CRL_DATA_FOLDER"/robots/simple/dog.rbs");

  RobotRB* selectedRB = nullptr; // robot rigid body selected by mouse, = NULL when nothing selected
  bool appIsRunning = false;

  timing timer;

  double simTime = 0; // current simulation time

  // UI Variables
  double testJointAngle = 0;
  std::vector<double> jointAngles;
  int selected_leg = 0;
  Vector3d lerp_A = { 0,-.08,0 };
  Vector3d lerp_B = { 0,-0.1,0 };
  double angle_A = 0;
  double angle_B = 0;
  double transition_time = 0.1302;
  double step_size = 0.07;
  double spider_radius = 0.23;
  double jump_velocity = 10; double flip_velocity = 4.0;
  Vector3d control_input = { 0, 0, 0 };
  double angle_input = 0;

  bool button_A_toggle = false;
  bool button_START_toggle = false;
  bool button_Y_toggle = false; 
  bool button_BACK_toggle = false;

  bool debug_autopause = false;
  bool auto_update_path = false;
  bool show_network = false;
  bool show_env = false;

  // example variables for how to use ImGui
  bool myBool = false;
  double myDouble = 1;
  double myDouble2 = 2;
  Vector3d myVector3d = {1, 2, 3};
  int myPlotCounter = 0;
  const static int MY_PLOT_N = 100;
  float myPlotValues[MY_PLOT_N];

  double rel_Angle;
  float rel_AnglePlotValues[MY_PLOT_N];

  double vel_Angle;
  float vel_AnglePlotValues[MY_PLOT_N];

  NodeNetwork network;

  // example for a 3d guizmo
  bool guizmoEnabled = true;
  Vector3d guizmoPosition = Vector3d{0, 0, 3};

  LowEffortEnvironment env = LowEffortEnvironment(P3D(-20, 0, -20), P3D(20, 0, 20));
  LowEffortWall wall = LowEffortWall(P3D(4.5, 0, 20), P3D(-20, 0, 3));

};
