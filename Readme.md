# SpiderPig

All presentation related material is in the subfolder "presentation". The .pptx was too big because the videos in there are encoded with a bad codec. They are also on my OneDrive, here are direct links:

Recording of the presentation: https://1drv.ms/v/s!Amb8hf4mLmNQrOshvHLWyQOw90VbTA?e=JMo1pg

Presentation with video (pptx): https://1drv.ms/p/s!Amb8hf4mLmNQrOsrM6vYQaqrDHFhkA?e=U1MK9K

Presentation without video (pdf): https://1drv.ms/b/s!Amb8hf4mLmNQrOsk0donbfxHbxlVeA?e=t269eM

## Team Members

| Legi Number | First Name | Last Name | github username |
| ----------- | ---------- | --------- | --------------- |
| 07-179-591  | Nathanael  | Köhler    | nathanael-k     |
